
set(PIP_INDEX "https://atlas-tdaq-repository.web.cern.ch/repository/pypi/" CACHE STRING "URL for PiPy proxy")
set(PIP_EXTRA_INDEX "https://atlas-software-dist-eos.web.cern.ch/atlas-software-dist-eos/externals/PyModules/repo/" CACHE STRING "ATLAS offline py repo")

# Set this option in local area during configuration to not pick up stuff from
# LCG or TDAQ install areas. Default is OFF.
option(PYTHON_PACKAGES_INSTALL_ALL "Install all python modules locally")

if(PYTHON_PACKAGES_INSTALL_ALL)
  set(PYTHON_INSTALL_FLAGS "-I")
endif()

# External Python packages not in LCG

find_package(Python QUIET COMPONENTS Interpreter)

set(TDAQ_EXTERNAL_PYTHON_MODULES)

# Note that the version numbers are in constraints.txt
# That file is given as a constraint to pip, so the
# order should no longer matter.

function(tdaq_add_external_python_modules)
  foreach(name ${ARGV})
    string(REGEX MATCH "[^[=<>]+" target ${name})
    ExternalProject_add(external-${target}
      EXCLUDE_FROM_ALL TRUE
      DOWNLOAD_COMMAND true
      CONFIGURE_COMMAND true
      BUILD_COMMAND true
      INSTALL_COMMAND  env -u PYTHONPATH PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} LD_LIBRARY_PATH=${LIBFFI_ROOT}/lib64:$ENV{LD_LIBRARY_PATH} ${Python_EXECUTABLE} -m pip --no-cache-dir install --no-warn-script-location --index-url ${PIP_INDEX} --extra-index-url ${PIP_EXTRA_INDEX} --user -c ${CMAKE_CURRENT_SOURCE_DIR}/constraints.txt ${PYTHON_INSTALL_FLAGS} ${name})
    list(APPEND TDAQ_EXTERNAL_PYTHON_MODULES external-${target})
  endforeach()
  set(TDAQ_EXTERNAL_PYTHON_MODULES ${TDAQ_EXTERNAL_PYTHON_MODULES} PARENT_SCOPE)
endfunction()

tdaq_add_external_python_modules(
  hep-crest-client
  pycrest-client
)

add_custom_command(OUTPUT requirements.txt
  COMMAND env -u PYTHONPATH PYTHONUSERBASE=${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG} ${Python_EXECUTABLE} -m pip freeze --user > requirements.txt
  COMMAND cp requirements.txt ${CMAKE_INSTALL_PREFIX}/external/${BINARY_TAG}/lib
  VERBATIM
  DEPENDS ${TDAQ_EXTERNAL_PYTHON_MODULES}
)

add_custom_target(external-python-packages
  ALL
  DEPENDS  requirements.txt
)

add_dependencies(tdaq-common-external-software ${TDAQ_EXTERNAL_PYTHON_MODULES})

